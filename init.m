clear all
clc
close all

% default
lambda = 1;
atilde = 0;


ac = 10;
acd = 0.1;

Actuator_Enable = 1; % 1 = off 0 = on
a = 1;
b = 1;

x0 = 2*0;
aR = -1;
bR = 1;

kx = (a-aR)/b;
kr = bR/b;

ktmax = 5;
ktmin = 0;
beta = 1;
gamma = 5;

alpha = 0;

um = 0;

rmax = 1;

% % atilde
% atilde = 0;
% um_max = atilde*abs((bR/aR)*rmax);
% ktstar = 1 + atilde/(b*kx);
% c = -(aR + (b*kx*(ktstar-1)^2)/(4*(ktmax-1)));
% Valpha = min(c,beta);
% R = (beta/(2*gamma))*(ktstar-1)^2 + um_max^2/c;


% % lambda
% lambda = 0.9;
um_max = b*abs(lambda-1)*(abs(kr)+abs(kx*(bR/aR)))*rmax;
ktstar = 1/lambda;
c = -(aR + (b*kx*lambda*(ktstar-1)^2)/(4*(ktmax-1)));
Valpha = min(c,beta);
R = (beta*lambda/(2*gamma))*(ktstar-1)^2 + um_max^2/c;




% R = (beta/(2*gamma))*(ktstar-1)^2 + um^2/c;